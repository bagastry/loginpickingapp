package com.example.bagastryambodo.loginpagepicking;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    LinearLayout l1, l2, l3, l4, l5, l6, l7, l8;
    Button btn;
    Animation downtoup;
    Animation uptodown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = findViewById(R.id.btnLogin);
        l1 = findViewById(R.id.linear1);
        l2 = findViewById(R.id.linear2);
        l3 = findViewById(R.id.linear3);
        l4 = findViewById(R.id.linear4);
        l5 = findViewById(R.id.linear5);
        l6 = findViewById(R.id.linear6);
        l7 = findViewById(R.id.linear7);
        l8 = findViewById(R.id.linear8);
        downtoup = AnimationUtils.loadAnimation(this, R.anim.downtoup);
        uptodown = AnimationUtils.loadAnimation(this, R.anim.uptodown);
        l1.setAnimation(downtoup);
        l2.setAnimation(downtoup);
        l3.setAnimation(downtoup);
        l4.setAnimation(uptodown);
        l5.setAnimation(uptodown);
        l6.setAnimation(uptodown);
        l7.setAnimation(uptodown);
        l8.setAnimation(uptodown);
    }
}
